using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PasarNivel : MonoBehaviour
{
   
    [SerializeField] AudioClip[] musicaWin;
    AudioSource audio1;
    [SerializeField] GameObject nuevocanvas;
    [SerializeField] GameObject botonSiguienteNivel;

    // Start is called before the first frame update
    void Start()
    {
        audio1 = GetComponent<AudioSource>();
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(botonSiguienteNivel);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            nuevocanvas.SetActive(true);         
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            audio1.Stop();
            audio1.clip = musicaWin[0];
            audio1.Play();
            nuevocanvas.SetActive(true);
            other.GetComponent<MovimientoPlayer>().enabled = false;
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(botonSiguienteNivel);


            
        }

    }
    public void Nivel1()
    {
        SceneManager.LoadScene("Nivel1");
        Debug.Log("Toca Objeto");
    }

    
    public void Menu()
    {
        SceneManager.LoadScene(0);
    }

    public void NextLevl()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
