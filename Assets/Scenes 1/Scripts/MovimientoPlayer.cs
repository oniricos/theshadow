using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Video;


public class MovimientoPlayer : MonoBehaviour
{
   
    public VideoPlayer video;
    [SerializeField] Text puntuacion;
    float h =1, v=1;
    [SerializeField] GameObject[] colection;
    Animator anim;
    CharacterController controller;
    [SerializeField] float velocidad = 50;
    float velRotacion = 10;
    Vector3 direccion;
    public float turnSmoothTime;
    float turnSmoothVelocity;
    int contColection;
    bool logro;
    AudioSource audio1;
    [SerializeField] AudioClip[] clips;
    [SerializeField] AudioSource music ;

    // Start is called before the first frame update
    void Start()
    {
       
      
       
        audio1 = GetComponent<AudioSource>();
        anim = GetComponentInChildren<Animator>();
        controller = GetComponent<CharacterController>();
        //colection = GameObject.FindGameObjectsWithTag("bonus");
        //puntuacion.text = "Coleccionables: " + contColection + " / " + colection.Length;
       
    }

    // Update is called once per frame
    void Update()
    {
        h = Input.GetAxisRaw("Horizontal");
        v = Input.GetAxisRaw("Vertical");
        direccion = new Vector3(v, 0, -h).normalized;
        controller.Move(direccion * velocidad * Time.deltaTime);
      

        //GirarADireccion();

        if (direccion.magnitude >= 0.1f)
        {
            audio1.clip = clips[0];
            audio1.Play();
            float targetAngle = Mathf.Atan2(direccion.x, direccion.z) * Mathf.Rad2Deg;

            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);

            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            controller.Move(direccion.normalized * velocidad * Time.deltaTime);
            anim.SetBool("andar", true);
         
        }
        else
        {
            anim.SetBool("andar", false);
        }
        if (contColection == colection.Length && !logro)
        {
            Debug.Log("�Conseguiste todos los coleccionables!");
            logro = true;
        }
    }

    void GirarADireccion()
    {
        Quaternion rotDestino = Quaternion.LookRotation(direccion);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotDestino, velRotacion * Time.deltaTime);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        /*if (other.gameObject.CompareTag("bonus"))
        {
            audio.clip = clips[1];
            audio.Play();
            Destroy(other.gameObject);
            Debug.Log("tocado");
            contColection++;
            puntuacion.text = "Coleccionables: " + contColection + " / " + colection.Length;
            Debug.Log("Llevas " + contColection + " coleccionables.");
        }*/
        if (other.gameObject.CompareTag("cajaFuerte"))
        {
            music.Pause();
            audio1.clip = clips[2];
            audio1.Play();
            anim.SetBool("andar", false);
        }
    }
}
