using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class ChangeScene : MonoBehaviour
{
    // Start is called before the first frame update
    VideoPlayer videoIntro;
    void Awake() 
    {
        videoIntro = GetComponent<VideoPlayer>();
        videoIntro.Play();
        videoIntro.loopPointReached += CheckOver;
    }
 
 
     void CheckOver(UnityEngine.Video.VideoPlayer vp)
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    }
    private void Update() {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}
