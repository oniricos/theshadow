using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class SlotInfo : MonoBehaviour
{
    static public int nivelesDesbloqueados;
    public int nivelActual;
    public Button[] botonesMenu;

    // Start is called before the first frame update
    void Start()
    {
        //if (SceneManager.GetActiveScene().name == "MenuNuevo 1")
        //{
        //    ActualizarBotones();
        //}
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CargarNivel(int nivel)
    {
        if (nivel == -1)
        {
            SceneManager.LoadScene("MenuNuevo");
        }
        if (nivel == 0)
        {
            SceneManager.LoadScene("MenuNuevo 1");
        }
        else
        {
            SceneManager.LoadScene(nivel);
        }
       
    }

    public void DesbloquearNivel()
    {
        if (nivelesDesbloqueados < nivelActual)
        {
            nivelesDesbloqueados = nivelActual;
            nivelActual++;
        }
    }

    //public void ActualizarBotones()
    //{
    //    for (int i = 0; i < nivelesDesbloqueados + 1; i++)
    //    {
    //        botonesMenu[i].interactable = true;
    //    }
    //}
}
