using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelRayos : MonoBehaviour
{
    [SerializeField] AudioClip[] sonidoRayo;
    [SerializeField] GameObject muerte;
    [SerializeField] int rango;
    RaycastHit hit;
    ParticleSystem particleSys;
    [SerializeField] int tiempoEspera;
    [SerializeField] int duracionRayo;
    [SerializeField] float contador;
    bool vueltas = true;
    AudioSource audio1;
     // Start is called before the first frame update
    void Start()
    {
        particleSys = GetComponent<ParticleSystem>();
        audio1 = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        contador += Time.deltaTime;
        if (contador <= duracionRayo)
        {
            DisparaRayos();
            particleSys.Play();
            //Debug.Log(contador);
        }
        else if (contador >= duracionRayo && vueltas)
        {
            particleSys.Stop();
            vueltas = false;
            StartCoroutine(TiempoEspera());
            //Debug.Log("estoy aqui");
        }

    }

    void DisparaRayos()
    {

        if (Physics.Raycast(gameObject.transform.position, gameObject.transform.forward, out hit, rango))
        {
            //Debug.Log(hit.collider.name);
            if (hit.collider.CompareTag("Player"))
            {
                muerte.SetActive(true);
                Destroy(hit.collider.gameObject);
                audio1.clip = sonidoRayo[0];
                audio1.Play();
            }
        }
    }
    IEnumerator TiempoEspera()
    {
        yield return new WaitForSeconds(tiempoEspera);
        contador = 0;
        vueltas = true;
    }
}