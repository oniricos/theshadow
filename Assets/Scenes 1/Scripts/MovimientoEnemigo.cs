using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class MovimientoEnemigo : MonoBehaviour
{
    [SerializeField] GameObject muerte;
    [SerializeField] AudioClip[] muertePlayer;
    //bool enCamino = false;
    NavMeshAgent agent;
    [SerializeField] Transform[] punto;
    int siguientePunto;
    Vector3 posInicial;
    CharacterController controlador;
    MovimientoPlayer player;
    AudioSource audio1;
    

    // Start is called before the first frame update
    void Start()
    {
        //muerte = GameObject.FindGameObjectWithTag("Cmuerte");
        player = GetComponent<MovimientoPlayer>();
        controlador = GetComponent<CharacterController>();
        posInicial = transform.position;
        agent = GetComponent<NavMeshAgent>();
        audio1 = GetComponent<AudioSource>();
        //StartCoroutine(MovimientoIA());
    }

    // Update is called once per frame
    void Update()
    {
        if (HemosLlegado())
        {
            DetenerNavMeshAgent();
            if (true)
            {

            }
            siguientePunto = (siguientePunto + 1) % punto.Length;
            ActualizarPuntoDestino();
        }
    }
    public void ActualizarPuntoDestinoNavMeshAgent(Vector3 puntoDestino)
    {
        agent.destination = puntoDestino;
        agent.isStopped = false;

      

    }
    void DetenerNavMeshAgent()
    {
        agent.isStopped = true;
    }
    public bool HemosLlegado()
    {
        return agent.remainingDistance <= agent.stoppingDistance && !agent.pathPending;
    }

    void OnEnable()
    {
        ActualizarPuntoDestino();
    }
    void ActualizarPuntoDestino()
    {
        ActualizarPuntoDestinoNavMeshAgent(punto[siguientePunto].position);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("TOUCH");
            muerte.gameObject.SetActive(true);
            Destroy(other.gameObject);
            audio1.clip = muertePlayer[0];
            audio1.Play();
            //Destroy(other.gameObject);
           
        }
    }
}

    //IEnumerator MovimientoIA()
    //{
    //    enCamino = true;
    //    while (true)
    //    {
    //        agent.SetDestination(punto.transform.position);
    //        if (transform.position==punto.transform.position)
    //        {
    //            yield return new WaitForSeconds(4f);
    //            agent.SetDestination(posInicial);
    //            if (transform.position==posInicial)
    //            {
    //                yield return new WaitForSeconds(4f);

    //            }

    //        }
    //        enCamino = false;
    //    }
    //}