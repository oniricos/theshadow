using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class Menu : MonoBehaviour
{
    GameManager gm;
    [SerializeField] GameObject botonSiguiente;
    // Start is called before the first frame update

    private void Awake() {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(botonSiguiente);
    }
    void Start()
    {
        gm = GetComponent<GameManager>();

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Jugar()
    {
        SceneManager.LoadScene(1);
    }
    public void CerrarJuego()
    {
        Application.Quit();
    }
    public void VolverMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void PasarNivel()
    {

        SceneManager.LoadScene(gm.scene + 1);
    }
}
