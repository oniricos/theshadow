using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Camara : MonoBehaviour
{
    GameObject playerSeguir;
    public Text tiempo;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        playerSeguir = GameObject.FindGameObjectWithTag("Player");
        //tiempo.text = "Tiempo: " + (int)Time.time + " seg";

    }

    void LateUpdate()
    {
        //1. Seguimiento de c�mara.
        if (playerSeguir != null)
        {
            transform.position = Vector3.Lerp(transform.position, playerSeguir.transform.position + new Vector3(-7, 10, 0), 3 * Time.deltaTime);
            //2. Clampeo (delimitar la c�mara entre ciertos m�rgenes).
            float xLimite = Mathf.Clamp(transform.position.x, -100f, 100f);
            float yLimite = Mathf.Clamp(transform.position.y, -0.87f, 200f);

            transform.position = new Vector3(xLimite, yLimite, transform.position.z);
        }
    }
}
