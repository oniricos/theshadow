using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] static bool juegoPausado = false;
    [SerializeField] GameObject pausarMenu;
    [SerializeField] GameObject botonReanudar;

void Awake() 
{

}

void Update()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            EventSystem.current.SetSelectedGameObject(null);
            if (juegoPausado)
            {

                Reanudar();
                EventSystem.current.SetSelectedGameObject(null);
                //EventSystem.current.SetSelectedGameObject(botonReanudar);


            }
            else
            {
                Pausa();
            }
        }

    }
    
    public void Reanudar()
    {
        pausarMenu.SetActive(false);
        Time.timeScale = 1f;
        juegoPausado = false;



    }

    void Pausa()
    {
        pausarMenu.SetActive(true);
        Time.timeScale = 0f;
        juegoPausado = true;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(botonReanudar);


    }

    public void Menu()
    {
        SceneManager.LoadScene(0);
        Reanudar();
        
    }

}
