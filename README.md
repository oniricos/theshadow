Un pequeño proyecto open source de infiltración en el que intentarás robar las posesiones más valiosas que tienen los demás gremios de Oniria. Basado en el mundo de Oniria World.
https://oniria.world/comunidad_/
https://oniria-world.fandom.com/es/wiki/Wiki_Oniria_World

Instrucciones para colaborar
1.- Clona este proyecto
2.- Crea una rama nueva
3.- Dentro de Unity, crea una carpeta para tu facción
4.- No modifiques ninguna escena que no hayas hecho tú y trabaja siempre dentro de tu carpeta
5.- Actualiza y sube tu rama
6.- Espera a que un admin la incorpore

Ayuda al desarrollo

Creditos

Modelado y Animación 3D: Alejandro Valderas, Alejandro Gil,  Adrián Pastrián , Victor Manuel,  Victor Ruiz

Programación: Victor Ruiz, Álvaro Rodrigo

Environment Artist: Alejandro Valderas

Productor y Game Designer:  Alberto Roldán
