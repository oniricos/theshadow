﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Camara::Start()
extern void Camara_Start_mB05F311183469B02F3CBDDBD6B3A2127026C5B25 (void);
// 0x00000002 System.Void Camara::Update()
extern void Camara_Update_m15441C8DAE4E153BA20C9BC717B17E452455A492 (void);
// 0x00000003 System.Void Camara::LateUpdate()
extern void Camara_LateUpdate_mFBB878E14CCB3A9EBCA2724600F002DCE9AF5482 (void);
// 0x00000004 System.Void Camara::.ctor()
extern void Camara__ctor_mFC3B10318637DC83F5F43594E736B965E3E4CF7F (void);
// 0x00000005 System.Void ChangeScene::Awake()
extern void ChangeScene_Awake_m74A88D3CED3017EAC276DA5E15E33CD637366364 (void);
// 0x00000006 System.Void ChangeScene::CheckOver(UnityEngine.Video.VideoPlayer)
extern void ChangeScene_CheckOver_mE26AB0A9A9995CD995EEC52B3BBC81C774EE7931 (void);
// 0x00000007 System.Void ChangeScene::Update()
extern void ChangeScene_Update_m988B53FF548D03F4C27AC8E930D4F133BD49CD6F (void);
// 0x00000008 System.Void ChangeScene::.ctor()
extern void ChangeScene__ctor_m31CB879A0944B416D16AA2130F9504E1ABAD6FA1 (void);
// 0x00000009 System.Void CursorLock::Start()
extern void CursorLock_Start_mE8D963E1F984C802F771B5E4127FF0E6935CDDB1 (void);
// 0x0000000A System.Void CursorLock::Update()
extern void CursorLock_Update_mE70360EB2974A5A8FB10D60FE1289D1374FF5062 (void);
// 0x0000000B System.Void CursorLock::.ctor()
extern void CursorLock__ctor_m3F845F28B05B8519E17464FCD38E694DEE4C0446 (void);
// 0x0000000C System.Void GameManager::Start()
extern void GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2 (void);
// 0x0000000D System.Void GameManager::Update()
extern void GameManager_Update_m7F29D8E933B8D21D2E67507979C0F12ACF87BB41 (void);
// 0x0000000E System.Void GameManager::.ctor()
extern void GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368 (void);
// 0x0000000F System.Void Menu::Awake()
extern void Menu_Awake_m35EDEB582FE868A32CBA5F96EB3B9E9C344B75AF (void);
// 0x00000010 System.Void Menu::Start()
extern void Menu_Start_mC49986718939F87924A1391044721CAC6E28919D (void);
// 0x00000011 System.Void Menu::Update()
extern void Menu_Update_m664984862D794DB3E17B79AFC35D12A9ED961B5D (void);
// 0x00000012 System.Void Menu::Jugar()
extern void Menu_Jugar_m0A56407A7BD1442489AC37E57D9AB642498B8977 (void);
// 0x00000013 System.Void Menu::CerrarJuego()
extern void Menu_CerrarJuego_mCAED5F6F5B7EBA400B0A1C3560B80218E419B025 (void);
// 0x00000014 System.Void Menu::VolverMenu()
extern void Menu_VolverMenu_m9647D6445FD5DC1C79B436540D5586B2215A3F47 (void);
// 0x00000015 System.Void Menu::PasarNivel()
extern void Menu_PasarNivel_m454517AAB8D5DB94D83BCBFBC000F63BBA14BD97 (void);
// 0x00000016 System.Void Menu::.ctor()
extern void Menu__ctor_mBEF2B96BC9E3D64E020EBE40FEF9CF25E6C3ED00 (void);
// 0x00000017 System.Void MovimientoEnemigo::Start()
extern void MovimientoEnemigo_Start_mCD43C4D8BE831BD85E9DC642443CC460F960FD37 (void);
// 0x00000018 System.Void MovimientoEnemigo::Update()
extern void MovimientoEnemigo_Update_m7317CE9812CFB41C4CE20B31812B3E602625DA49 (void);
// 0x00000019 System.Void MovimientoEnemigo::ActualizarPuntoDestinoNavMeshAgent(UnityEngine.Vector3)
extern void MovimientoEnemigo_ActualizarPuntoDestinoNavMeshAgent_mE12D3884CE8C20B695EBBE07F81E824A0AD5E3F7 (void);
// 0x0000001A System.Void MovimientoEnemigo::DetenerNavMeshAgent()
extern void MovimientoEnemigo_DetenerNavMeshAgent_m3376B78B5B594FAB6E4023A8AD1380280A85B413 (void);
// 0x0000001B System.Boolean MovimientoEnemigo::HemosLlegado()
extern void MovimientoEnemigo_HemosLlegado_m796FD2B7FC27B9531B0D29C614DBCFB51E3F5BCB (void);
// 0x0000001C System.Void MovimientoEnemigo::OnEnable()
extern void MovimientoEnemigo_OnEnable_mCDBE93EA17DAB87253B09403D45FBAC3350E7635 (void);
// 0x0000001D System.Void MovimientoEnemigo::ActualizarPuntoDestino()
extern void MovimientoEnemigo_ActualizarPuntoDestino_m478CC5E6CF9E1B926D605EFD44385C1414F6BD52 (void);
// 0x0000001E System.Void MovimientoEnemigo::OnTriggerEnter(UnityEngine.Collider)
extern void MovimientoEnemigo_OnTriggerEnter_m9102FCA34B3B0AFF957F8D8BDEFD04A172FB802A (void);
// 0x0000001F System.Void MovimientoEnemigo::.ctor()
extern void MovimientoEnemigo__ctor_mDAEC0EAD2FFCBB104047CF4CA5262BDD5C4F29CF (void);
// 0x00000020 System.Void MovimientoPlayer::Start()
extern void MovimientoPlayer_Start_m0F5C42FAE9533BF1E59DC8C0EFA14708F98F444E (void);
// 0x00000021 System.Void MovimientoPlayer::Update()
extern void MovimientoPlayer_Update_m13FB64A08CC75470D27CEC8AC52BAE6A760F71FC (void);
// 0x00000022 System.Void MovimientoPlayer::GirarADireccion()
extern void MovimientoPlayer_GirarADireccion_m1AE5CBB0AD8056DE621703CBAA849B44FDE89598 (void);
// 0x00000023 System.Void MovimientoPlayer::OnTriggerEnter(UnityEngine.Collider)
extern void MovimientoPlayer_OnTriggerEnter_mEFF1E6E7C087E59A291FEB0E956C7FAEE3CD50DB (void);
// 0x00000024 System.Void MovimientoPlayer::.ctor()
extern void MovimientoPlayer__ctor_m07866A922A596B3BFA40C3A615940AA292418EB7 (void);
// 0x00000025 System.Void PanelRayos::Start()
extern void PanelRayos_Start_mB43C355D3FE8219DD8559E5E4423AEE390F394F4 (void);
// 0x00000026 System.Void PanelRayos::Update()
extern void PanelRayos_Update_m2C31F4B8DE95FC7D70B20012FCEA824FC0E392DA (void);
// 0x00000027 System.Void PanelRayos::DisparaRayos()
extern void PanelRayos_DisparaRayos_mED20C63F38854238DCBAF3153D66F352697D87E5 (void);
// 0x00000028 System.Collections.IEnumerator PanelRayos::TiempoEspera()
extern void PanelRayos_TiempoEspera_mE2A1065C633125C26D4B141E48CBFA1151B61ABE (void);
// 0x00000029 System.Void PanelRayos::.ctor()
extern void PanelRayos__ctor_m7CE217580CE81C0C74C9FCB810A8D7FB430405D7 (void);
// 0x0000002A System.Void PanelRayos/<TiempoEspera>d__13::.ctor(System.Int32)
extern void U3CTiempoEsperaU3Ed__13__ctor_mC5FECD65160D9E93D7894EF7E66A389BFE8AA59D (void);
// 0x0000002B System.Void PanelRayos/<TiempoEspera>d__13::System.IDisposable.Dispose()
extern void U3CTiempoEsperaU3Ed__13_System_IDisposable_Dispose_mC512FD07FA659020FDD551791911ED5EB49E8354 (void);
// 0x0000002C System.Boolean PanelRayos/<TiempoEspera>d__13::MoveNext()
extern void U3CTiempoEsperaU3Ed__13_MoveNext_mE7B2BF63637C72558826FC9B89E853975E2E2F61 (void);
// 0x0000002D System.Object PanelRayos/<TiempoEspera>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTiempoEsperaU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m705D4A317A42981C5B755805FEEC415E77BEB0D7 (void);
// 0x0000002E System.Void PanelRayos/<TiempoEspera>d__13::System.Collections.IEnumerator.Reset()
extern void U3CTiempoEsperaU3Ed__13_System_Collections_IEnumerator_Reset_mAC950B535B494CBADFF1974907959DDB3084E2B8 (void);
// 0x0000002F System.Object PanelRayos/<TiempoEspera>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CTiempoEsperaU3Ed__13_System_Collections_IEnumerator_get_Current_m68BF8C6772E851A410DB894F1B6CA0CE99972E2D (void);
// 0x00000030 System.Void PasarNivel::Start()
extern void PasarNivel_Start_m6CBCA662DC3A50E54EBC47CEFDE26B15911BD788 (void);
// 0x00000031 System.Void PasarNivel::Update()
extern void PasarNivel_Update_m49C71A177610620C8E7B16C04656309BB3E3368D (void);
// 0x00000032 System.Void PasarNivel::OnTriggerEnter(UnityEngine.Collider)
extern void PasarNivel_OnTriggerEnter_m6A07E264278AA56F4B19F5D2776E6154CE6734E2 (void);
// 0x00000033 System.Void PasarNivel::Nivel1()
extern void PasarNivel_Nivel1_mF0350C2AEBCA12B65D5787719FBCBA7E9CBA415E (void);
// 0x00000034 System.Void PasarNivel::Menu()
extern void PasarNivel_Menu_mA811666B67C8BFD8AAC102DFF9AA92AF81DCA39B (void);
// 0x00000035 System.Void PasarNivel::NextLevl()
extern void PasarNivel_NextLevl_m8E9956BF332C90C05A3A284CAA5890BA50922544 (void);
// 0x00000036 System.Void PasarNivel::.ctor()
extern void PasarNivel__ctor_m2422FCA1C68B8AD62FA67F94681D9B4D7208F1B3 (void);
// 0x00000037 System.Void PauseMenu::Awake()
extern void PauseMenu_Awake_mD17C2664126E61F76112D3C96B5A4684E0D00B66 (void);
// 0x00000038 System.Void PauseMenu::Update()
extern void PauseMenu_Update_m5097E74BFD4385B73BA9EF7198886DCEC7DF9A83 (void);
// 0x00000039 System.Void PauseMenu::Reanudar()
extern void PauseMenu_Reanudar_m9B87A6A5B09AAAE97C1D23D97B30A222B0077779 (void);
// 0x0000003A System.Void PauseMenu::Pausa()
extern void PauseMenu_Pausa_mAF3FD87617E7F02E10B8D534A0CAAF2E39EAE5E2 (void);
// 0x0000003B System.Void PauseMenu::Menu()
extern void PauseMenu_Menu_m13496C75FFCD8179064893A4D81103F02D00D604 (void);
// 0x0000003C System.Void PauseMenu::.ctor()
extern void PauseMenu__ctor_m81B0E020DC5008DA4D414200BAAF7122B430D826 (void);
// 0x0000003D System.Void SlotInfo::Start()
extern void SlotInfo_Start_m5B5C6311E9D6ED58B9A379A71E01B3340CAF7DBB (void);
// 0x0000003E System.Void SlotInfo::Update()
extern void SlotInfo_Update_m2CDBD295CC87498B63503E73046280EB5160E147 (void);
// 0x0000003F System.Void SlotInfo::CargarNivel(System.Int32)
extern void SlotInfo_CargarNivel_mB3B75CCE6FFD720111325A5731A3A9B004AAC1B4 (void);
// 0x00000040 System.Void SlotInfo::DesbloquearNivel()
extern void SlotInfo_DesbloquearNivel_m538013AD8A95AE3945DDF32A01450E6CA1E28C89 (void);
// 0x00000041 System.Void SlotInfo::.ctor()
extern void SlotInfo__ctor_mFF131772649547F280026F25A70BF2305E7360E1 (void);
static Il2CppMethodPointer s_methodPointers[65] = 
{
	Camara_Start_mB05F311183469B02F3CBDDBD6B3A2127026C5B25,
	Camara_Update_m15441C8DAE4E153BA20C9BC717B17E452455A492,
	Camara_LateUpdate_mFBB878E14CCB3A9EBCA2724600F002DCE9AF5482,
	Camara__ctor_mFC3B10318637DC83F5F43594E736B965E3E4CF7F,
	ChangeScene_Awake_m74A88D3CED3017EAC276DA5E15E33CD637366364,
	ChangeScene_CheckOver_mE26AB0A9A9995CD995EEC52B3BBC81C774EE7931,
	ChangeScene_Update_m988B53FF548D03F4C27AC8E930D4F133BD49CD6F,
	ChangeScene__ctor_m31CB879A0944B416D16AA2130F9504E1ABAD6FA1,
	CursorLock_Start_mE8D963E1F984C802F771B5E4127FF0E6935CDDB1,
	CursorLock_Update_mE70360EB2974A5A8FB10D60FE1289D1374FF5062,
	CursorLock__ctor_m3F845F28B05B8519E17464FCD38E694DEE4C0446,
	GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2,
	GameManager_Update_m7F29D8E933B8D21D2E67507979C0F12ACF87BB41,
	GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368,
	Menu_Awake_m35EDEB582FE868A32CBA5F96EB3B9E9C344B75AF,
	Menu_Start_mC49986718939F87924A1391044721CAC6E28919D,
	Menu_Update_m664984862D794DB3E17B79AFC35D12A9ED961B5D,
	Menu_Jugar_m0A56407A7BD1442489AC37E57D9AB642498B8977,
	Menu_CerrarJuego_mCAED5F6F5B7EBA400B0A1C3560B80218E419B025,
	Menu_VolverMenu_m9647D6445FD5DC1C79B436540D5586B2215A3F47,
	Menu_PasarNivel_m454517AAB8D5DB94D83BCBFBC000F63BBA14BD97,
	Menu__ctor_mBEF2B96BC9E3D64E020EBE40FEF9CF25E6C3ED00,
	MovimientoEnemigo_Start_mCD43C4D8BE831BD85E9DC642443CC460F960FD37,
	MovimientoEnemigo_Update_m7317CE9812CFB41C4CE20B31812B3E602625DA49,
	MovimientoEnemigo_ActualizarPuntoDestinoNavMeshAgent_mE12D3884CE8C20B695EBBE07F81E824A0AD5E3F7,
	MovimientoEnemigo_DetenerNavMeshAgent_m3376B78B5B594FAB6E4023A8AD1380280A85B413,
	MovimientoEnemigo_HemosLlegado_m796FD2B7FC27B9531B0D29C614DBCFB51E3F5BCB,
	MovimientoEnemigo_OnEnable_mCDBE93EA17DAB87253B09403D45FBAC3350E7635,
	MovimientoEnemigo_ActualizarPuntoDestino_m478CC5E6CF9E1B926D605EFD44385C1414F6BD52,
	MovimientoEnemigo_OnTriggerEnter_m9102FCA34B3B0AFF957F8D8BDEFD04A172FB802A,
	MovimientoEnemigo__ctor_mDAEC0EAD2FFCBB104047CF4CA5262BDD5C4F29CF,
	MovimientoPlayer_Start_m0F5C42FAE9533BF1E59DC8C0EFA14708F98F444E,
	MovimientoPlayer_Update_m13FB64A08CC75470D27CEC8AC52BAE6A760F71FC,
	MovimientoPlayer_GirarADireccion_m1AE5CBB0AD8056DE621703CBAA849B44FDE89598,
	MovimientoPlayer_OnTriggerEnter_mEFF1E6E7C087E59A291FEB0E956C7FAEE3CD50DB,
	MovimientoPlayer__ctor_m07866A922A596B3BFA40C3A615940AA292418EB7,
	PanelRayos_Start_mB43C355D3FE8219DD8559E5E4423AEE390F394F4,
	PanelRayos_Update_m2C31F4B8DE95FC7D70B20012FCEA824FC0E392DA,
	PanelRayos_DisparaRayos_mED20C63F38854238DCBAF3153D66F352697D87E5,
	PanelRayos_TiempoEspera_mE2A1065C633125C26D4B141E48CBFA1151B61ABE,
	PanelRayos__ctor_m7CE217580CE81C0C74C9FCB810A8D7FB430405D7,
	U3CTiempoEsperaU3Ed__13__ctor_mC5FECD65160D9E93D7894EF7E66A389BFE8AA59D,
	U3CTiempoEsperaU3Ed__13_System_IDisposable_Dispose_mC512FD07FA659020FDD551791911ED5EB49E8354,
	U3CTiempoEsperaU3Ed__13_MoveNext_mE7B2BF63637C72558826FC9B89E853975E2E2F61,
	U3CTiempoEsperaU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m705D4A317A42981C5B755805FEEC415E77BEB0D7,
	U3CTiempoEsperaU3Ed__13_System_Collections_IEnumerator_Reset_mAC950B535B494CBADFF1974907959DDB3084E2B8,
	U3CTiempoEsperaU3Ed__13_System_Collections_IEnumerator_get_Current_m68BF8C6772E851A410DB894F1B6CA0CE99972E2D,
	PasarNivel_Start_m6CBCA662DC3A50E54EBC47CEFDE26B15911BD788,
	PasarNivel_Update_m49C71A177610620C8E7B16C04656309BB3E3368D,
	PasarNivel_OnTriggerEnter_m6A07E264278AA56F4B19F5D2776E6154CE6734E2,
	PasarNivel_Nivel1_mF0350C2AEBCA12B65D5787719FBCBA7E9CBA415E,
	PasarNivel_Menu_mA811666B67C8BFD8AAC102DFF9AA92AF81DCA39B,
	PasarNivel_NextLevl_m8E9956BF332C90C05A3A284CAA5890BA50922544,
	PasarNivel__ctor_m2422FCA1C68B8AD62FA67F94681D9B4D7208F1B3,
	PauseMenu_Awake_mD17C2664126E61F76112D3C96B5A4684E0D00B66,
	PauseMenu_Update_m5097E74BFD4385B73BA9EF7198886DCEC7DF9A83,
	PauseMenu_Reanudar_m9B87A6A5B09AAAE97C1D23D97B30A222B0077779,
	PauseMenu_Pausa_mAF3FD87617E7F02E10B8D534A0CAAF2E39EAE5E2,
	PauseMenu_Menu_m13496C75FFCD8179064893A4D81103F02D00D604,
	PauseMenu__ctor_m81B0E020DC5008DA4D414200BAAF7122B430D826,
	SlotInfo_Start_m5B5C6311E9D6ED58B9A379A71E01B3340CAF7DBB,
	SlotInfo_Update_m2CDBD295CC87498B63503E73046280EB5160E147,
	SlotInfo_CargarNivel_mB3B75CCE6FFD720111325A5731A3A9B004AAC1B4,
	SlotInfo_DesbloquearNivel_m538013AD8A95AE3945DDF32A01450E6CA1E28C89,
	SlotInfo__ctor_mFF131772649547F280026F25A70BF2305E7360E1,
};
static const int32_t s_InvokerIndices[65] = 
{
	3238,
	3238,
	3238,
	3238,
	3238,
	2625,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	2690,
	3238,
	3100,
	3238,
	3238,
	2625,
	3238,
	3238,
	3238,
	3238,
	2625,
	3238,
	3238,
	3238,
	3238,
	3156,
	3238,
	2608,
	3238,
	3100,
	3156,
	3238,
	3156,
	3238,
	3238,
	2625,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	3238,
	2608,
	3238,
	3238,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	65,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
